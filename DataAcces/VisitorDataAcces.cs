﻿using System.Collections.Generic;
using System.Threading.Tasks;
using darwin_outlook_model;
using MongoDB.Bson;
using MongoDB.Driver;

namespace darwin_visitor.DataAcces
{
    public class VisitorDataAcces
    {
        MongoClient _client;
        IMongoDatabase _db;

        public VisitorDataAcces()
        {
            _client = new MongoClient("mongodb://0.tcp.ngrok.io:10442");
            _db = _client.GetDatabase("Darwin");
        }

        public IEnumerable<VisitorModel> GetMeetingModels()
        {
            return _db.GetCollection<VisitorModel>("MeetingModels").Find(_ => true).ToList();
        }


        public async Task<VisitorModel> GetMeetingModel(ObjectId id)
        {
            var x = await _db.GetCollection<VisitorModel>("VisitorModels").Find(p => ObjectId.Parse( p.Id) == id).SingleAsync();

            return x;
        }

        public async Task<VisitorModel> Create(VisitorModel p)
        {
            await _db.GetCollection<VisitorModel>("VisitorModels").InsertOneAsync(p);
            return p;
        }

        public async void Update(ObjectId id, VisitorModel p)
        {
            p.Id = id.ToString();
            await _db.GetCollection<VisitorModel>("VisitorModels").ReplaceOneAsync(item => item.Id == p.Id, p, new UpdateOptions { IsUpsert = true });

        }
        public async void Remove(ObjectId id)
        {
            await _db.GetCollection<VisitorModel>("VisitorModels").DeleteOneAsync(item => ObjectId.Parse(item.Id) == id);
        }
    }
}