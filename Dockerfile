FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 61587
EXPOSE 44326

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY ["../darwin-visitor/darwin-visitor.csproj", "../darwin-visitor/"]
RUN dotnet restore "../darwin-visitor/darwin-visitor.csproj"
COPY . .
WORKDIR "/src/../darwin-visitor"
RUN dotnet build "darwin-visitor.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "darwin-visitor.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "darwin-visitor.dll"]